;;; guix-qa-frontpage
;;;
;;; Copyright © 2022 Christopher Baines <mail@cbaines.net>
;;;
;;; This file is part of guix-qa-frontpage.
;;;
;;; guix-qa-frontpage is free software; you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; guix-qa-frontpage is distributed in the hope that it will be
;;; useful, but WITHOUT ANY WARRANTY; without even the implied
;;; warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
;;; See the GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with the guix-qa-frontpage.  If not, see
;;; <http://www.gnu.org/licenses/>.

;;; Run the following command to enter a development environment for
;;; the guix-qa-frontpage:
;;;
;;;  $ guix environment -l guix-dev.scm

(use-modules ((guix licenses) #:prefix license:)
             (guix packages)
             (guix download)
             (guix git-download)
             (guix utils)
             (guix build-system gnu)
             (gnu packages)
             (gnu packages autotools)
             (gnu packages databases)
             (gnu packages gnupg)
             (gnu packages guile)
             (gnu packages web)
             (gnu packages guile-xyz)
             (gnu packages package-management)
             (gnu packages pkg-config)
             (gnu packages texinfo)
             (gnu packages ruby)
             (srfi srfi-1))

(package
  (name "guix-qa-frontpage")
  (version "0.0.0")
  (source #f)
  (build-system gnu-build-system)
  (inputs
   (list guix
         guix-data-service
         guile-json-4
         guile-fibers-1.1
         guile-knots
         guile-kolam
         guile-git
         guile-debbugs
         guile-readline
         guile-prometheus
         guix-build-coordinator
         guile-3.0-latest))
  (native-inputs
   (list autoconf
         automake
         pkg-config))
  (synopsis "TODO")
  (description "TODO")
  (home-page "TODO")
  (license license:gpl3+))
