;;; Guix QA Frontpage
;;;
;;; Copyright © 2023 Christopher Baines <mail@cbaines.net>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU Affero General Public License
;;; as published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (guix-qa-frontpage utils)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-71)
  #:use-module (ice-9 match)
  #:use-module (ice-9 threads)
  #:use-module (fibers)
  #:use-module (fibers channels)
  #:use-module (knots)
  #:use-module (zlib)
  #:export (non-blocking
            call-with-zlib-input-port*))

(define (non-blocking thunk)
  (let ((channel (make-channel)))
    (call-with-default-io-waiters
     (lambda ()
       (call-with-new-thread
        (lambda ()
          (with-exception-handler
              (lambda (exn)
                (put-message channel `(exception ,exn)))
            (lambda ()
              (with-throw-handler #t
                (lambda ()
                  (call-with-values thunk
                    (lambda values
                      (put-message channel `(values ,@values)))))
                (lambda args
                  (display (backtrace) (current-error-port))
                  (newline (current-error-port)))))
            #:unwind? #t)))))
    (match (get-message channel)
      (('values . results)
       (apply values results))
      (('exception . exn)
       (raise-exception exn)))))

(define* (call-with-zlib-input-port* port proc
                                     #:key
                                     (format 'zlib)
                                     (buffer-size %default-buffer-size))
  "Call PROC with a port that wraps PORT and decompresses data read from it.
PORT is closed upon completion.  The zlib internal buffer size is set to
BUFFER-SIZE bytes."
  (let ((zlib (make-zlib-input-port port
                                    #:format format
                                    #:buffer-size buffer-size
                                    #:close? #t)))
    (call-with-values
        (lambda ()
          (proc zlib))
      (lambda vals
        (close-port zlib)
        (apply values vals)))))
