(define-module (guix-qa-frontpage view reproducible-builds)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 match)
  #:use-module (ice-9 string-fun)
  #:use-module (guix-qa-frontpage view util)
  #:use-module (guix-qa-frontpage view shared)
  #:export (reproducible-builds-view))

(define (reproducible-builds-view package-reproducibility
                                  issue-data)
  (layout
   #:title "Reproducible builds"
   #:body
   `((main
      (p "The following table gives an overview of packages that can be built
reproducibly, as well as known issues.  All data is from the master branch.")

      ,(package-reproducibility-table package-reproducibility)

      (h3 "Issues")
      (table
       (thead
        (tr
         (th "Issue")
         (th "Title")
         (th "Status")))
       (tbody
        ,@(map
           (match-lambda
             ((issue-number . details)
              `(tr
                (td (a (@ (href
                           ,(simple-format #f "https://issues.guix.gnu.org/~A"
                                           issue-number)))
                       ,issue-number))
                (td ,(assq-ref details 'title))
                (td ,(if (assq-ref details 'open?)
                         "Open"
                         "Closed")))))
           issue-data)))))))
