(define-module (guix-qa-frontpage view package)
  #:use-module (guix-qa-frontpage view util)
  #:export (package-view))

(define (package-view package-data)
  (define name (assoc-ref package-data "name"))

  (layout
   #:title (string-append "Package: " name)
   #:body
   `((main
      (dt "Synopsis")
      (dd (raw ,(assoc-ref (assoc-ref package-data "synopsis") "html")))
      (dt "Description")
      (dd (raw ,(assoc-ref (assoc-ref package-data "description") "html")))
      (dt "Home page")
      (dd (a (@ (href ,(assoc-ref package-data "home-page")))
             ,(assoc-ref package-data "home-page")))

      (h1 "TODO More information to come")))))
