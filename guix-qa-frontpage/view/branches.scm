(define-module (guix-qa-frontpage view branches)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 match)
  #:use-module (guix-qa-frontpage view util)
  #:export (branches-view))

(define (branches-view branches)
  (layout
   #:title "Branches"
   #:body
   `((main
      (table
       (thead
        (tr (th "Branch")
            (th "Request to merge")))
       (tbody
        ,@(map (match-lambda
                 ((name . details)
                  (let ((issue-number
                         (assoc-ref details "issue_number")))
                    `(tr
                      (td (a (@ (href ,(simple-format #f "/branch/~A" name))
                                (style "font-family: monospace;"))
                             ,name))
                      (td ,@(if issue-number
                                `((a (@ (href ,(string-append
                                                "https://issues.guix.gnu.org/"
                                                (number->string issue-number))))
                                     "#" ,issue-number))
                                '()))))))
               branches)))))))
