(define-module (guix-qa-frontpage view home)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 match)
  #:use-module (guix-qa-frontpage view util)
  #:export (home
            readme))

(define (home branches)
  (layout
   #:description "Guix Quality Assurance"
   #:head
   '((style "
dt:before {
    content: "";
    display: block;
}
dt, dd {
    display: inline;
}
dd {
    margin-left: 0.6em;
}
dt {
    margin-left: 2em;
}

td.bad {
  padding: 0.05rem 0.65rem;
  font-weight: bold;

  border: 0.3rem dashed red;
}
"))
   #:body
   `((main
      (div
       (@ (class "row"))
       (section
        (a (@ (href "/branch/master"))
           (h2 "branch: master"))))

      (h2 ,(gettext "Patches" "guix-qa-frontpage"))
      (div
       (@ (class "row"))
       (section
        (a (@ (href "/patches")
              (style "font-size: 140%;"))
           "List of issues for patches")

        (div
         (h3 "Statistics")
         (p
          (a
           (@ (href "https://debbugs.gnu.org/rrd/guix-patches.html"))
           (img (@ (width "60%")
                   (src "https://debbugs.gnu.org/rrd/guix-patches_1m.png"))))

          (dl
           (@ (style "margin-top: 0;"))
           (dt "CR")
           (dd "serious,grave,critical")

           (dt "IM")
           (dd "important")

           (dt "NO")
           (dd "normal")

           (dt "MI")
           (dd "minor")

           (dt "WI")
           (dd "wishlist"))))))

      (h2 "Branches")
      (div
       (@ (class "row"))
       (section
        (table
         (@ (style "max-height: 20em; overflow-y: scroll;"))
         (thead
          (tr (th "Branch")
              (th "Request to merge")))
         (tbody
          ,@(if (assq-ref branches 'exception)
                `((tr
                   (td (@ (colspan 2) (class "bad")
                          (style "white-space: normal;"))
                       "Exception fetching branches:"
                       (br)
                       ,(assq-ref branches 'exception))))

                (append-map
                 (match-lambda
                   ((branch . details)
                    (let ((issue-number
                           (assoc-ref details "issue_number")))
                      `((tr
                         (td (a (@ (href ,(string-append "/branch/" branch))
                                   (style "font-family: monospace;"))
                                ,branch))
                         (td ,@(if issue-number
                                   `((a (@ (href ,(string-append
                                                   "https://issues.guix.gnu.org/"
                                                   (number->string issue-number))))
                                        "#" ,issue-number))
                                   '())))))))
                 branches))))))
      (h2 "Topics")
      (div
       (@ (class "row"))
       (p
        (img (@ (src "/assets/img/reproducible_builds.svg")
                (alt "Reproducible Builds"))))

       (p
        (a (@ (href "/reproducible-builds"))
           "Find out more about Reproducible Builds in Guix")))

      (p
       (@ (style "width: unset; text-align: center;"))
       "qa.guix.gnu.org is powered by the Guix qa-frontpage."
       (br)
       "Find the code in "
       (a (@ (href "https://git.savannah.gnu.org/cgit/guix/qa-frontpage.git/"))
          "this git repository")
       " and send patches to "
       (a (@ (href "mailto:guix-devel@gnu.org"))
          "guix-devel@gnu.org")
       ".")))))

(define (readme contents)
  (layout
   #:description "Guix Quality Assurance"
   #:body
   `((main
      (raw ,contents)))))
