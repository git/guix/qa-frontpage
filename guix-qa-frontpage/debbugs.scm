;;; Guix QA Frontpage
;;;
;;; Copyright © 2023 Christopher Baines <mail@cbaines.net>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU Affero General Public License
;;; as published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (guix-qa-frontpage debbugs)
  #:use-module (debbugs)
  #:use-module (guix-qa-frontpage mumi)
  #:export (debbugs-get-issues-with-guix-usertag

            fetch-issues-with-guix-tag))

(define (debbugs-get-issues-with-guix-usertag)
  ;; TODO Ideally this would be non-blocking
  (soap-invoke (%gnu) get-usertag "guix"))

(define (fetch-issues-with-guix-tag tag)
  (define issues
    (let ((data (assoc-ref (debbugs-get-issues-with-guix-usertag)
                           tag)))
      (if (number? data)
          (list data)
          data)))

  (map cons
       issues
       (mumi-bulk-issues issues)))
