(define-module (guix-qa-frontpage package)
  #:use-module (srfi srfi-1)
  #:use-module (guix-qa-frontpage guix-data-service)
  #:export (package-data))

(define (package-data name)
  (let* ((revision (get-latest-processed-branch-revision "master"))
         (package-versions
          (vector->list
           (assoc-ref
            (guix-data-service-request
             (string-append
              "https://data.qa.guix.gnu.org/revision/" revision
              "/package/" name ".json"))
            "versions")))
         (data
          (guix-data-service-request
           (string-append
            "https://data.qa.guix.gnu.org/revision/" revision
            "/package/" name "/" (last package-versions) ".json"))))
    (peek data)))
